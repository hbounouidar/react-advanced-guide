// React, { useState } from 'react'; // hook
import React, { Component } from 'react';
import classes from './App.css';
// import Radium,{StyleRoot} from 'radium';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import withClass from '../hoc/withClass';
import Aux from '../hoc/Auxi';
import AuthContext from '../context/auth-context';




// avec stat
class App extends Component {
  constructor(props){
    super(props);
    console.log('[app.js] constructor');
    this.state = {
      persons : [
        {id:'A1', name : 'Max', age : 28},
        {id:'A2', name : 'hamza', age : 18},
        {id:'A3' ,name : 'hamzao', age : 8}
      ],
      showPersons:false,
      typedText : 'ham',
      changeCounter : 0,
      authenticated : false
     
    }
  }

static getDerivedStateFromProps(props,state){
  console.log('[app.js] getDerivedStateFromProps', props);
  return state;
}

componentDidMount() {
  console.log('[App.js] componentDidMount');
}


  deletePersonHandler = (personIndex) => {
    //const persons = this.state.persons.slice(); ES5
    const persons = [...this.state.persons];
    persons.splice(personIndex,1);
    this.setState({persons:persons});
  }

  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id ===id;
    })

    const person = {
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;
    // or const person = Object.assign({},this.state.persons[personIndex])
    this.setState( (prevState,props)=> {
        return {
          persons : persons,
          changeCounter: prevState.changeCounter+1
        }
      }
     )
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons : !doesShow});
  }

  countTypedHandler = (event)=> {

    this.setState({typedText :event.target.value});
  }

  deleteCharHandler = (index) => {
    const tmpString = this.state.typedText.split('');
    tmpString.splice(index,1);
    const updatedText = tmpString.join('');
    this.setState({typedText : updatedText});
    
  }

  loginHandler= () => {
    this.setState({authenticated:true});
  };

  render() {
    let persons = null;
    if (this.state.showPersons) {
      persons = (
             <Persons persons= {this.state.persons}
                clicked ={this.deletePersonHandler}
                changed={this.nameChangeHandler}
                isAuthenticated = {this.state.authenticated}
                />     
      );
    }
    return (
      <AuthContext.Provider value={{
          authenticated : this.state.authenticated,
          login : this.loginHandler
         }}>
        <Aux classes={classes.App}>
          <Cockpit 
            title = {this.props.appTitle}
            showPersons={this.state.showPersons}
            personsLength={this.state.persons.length}
            clicked={this.togglePersonsHandler}
            />
          {persons}         
        </Aux>
      </AuthContext.Provider>
    ); 
    //return React.createElement('div',null,React.createElement('h1',null,'is that work !'));
  } 
}
export default withClass(App, classes.App);

