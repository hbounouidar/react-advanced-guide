import React from 'react';
import classes from './Person.css';


const person = (props) => {
/*     const style = {
        '@media (min-width : 500px)': {
            width:'450px'
        }
    } */
    return (
        // <div className="Person" style={style}>
        <div className={classes.Person}>
            <p onClick={props.click}>I'm a {props.name} and i am {props.age}!</p>
            <p>{props.children}</p>
            <input type="text" onChange={props.changed} value={props.name}/>
        </div>
    
    )
}

// export default Radium(person);
export default person;