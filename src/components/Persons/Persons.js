import React,{ Component} from 'react';
import Person from './Person/Person';

class Persons extends Component {
/*     static getDerivedStateFromProps(propse,state){

        return state;
    } */



    shoulComponentUpdate(nextProps, nextState){
        if (nextProps.persons !== this.props.persons){
            return true;
        }else {
            return false;
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState){
        return {message :'snaphot'};
    }
    componentDidUpdate(prevProps, prevState,snaphot){

    }

    componentWillUnmount() {
        //clean up
    }

    render (){
        return this.props.persons.map((person, index) => {
                return (<Person 
                         click={() => this.props.clicked(index)}
                         name={person.name} 
                         age={person.age} 
                         key={person.id}
                         changed = {(event) => this.props.changed(event,person.id)}
                         
                         />
                        );
               });

        
    }
}


export default Persons;