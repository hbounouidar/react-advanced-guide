import React, { Component } from 'react';
import PropTypes from 'prop-types';

// import Radium from 'radium';
import Aux from '../../../hoc/Auxi';
import withClass from '../../../hoc/withClass';
import classes from './Person.css';
import AuthContext from '../../../context/auth-context';

class Person extends Component {
    constructor(props) {
        super(props);
        this.inputElementRef=React.createRef();
    }

    static contextType = AuthContext; //pr y acceder 

    componentDidMount(){
       // this.myInput.focus();
        this.inputElementRef.current.focus();

    
    }
    render () {
        return(
            <Aux>
                {this.context.authenticated ? <p>Authenticated</p> : <p>Please Log in</p>}
                <p onClick={this.props.click}>
                  I m a {this.props.name} and i am {this.props.age}!</p>
                <p>{this.props.children}</p>
                <input 
                
                    // ref={(inputEl) => {this.myInput = inputEl}}
                    ref = {this.inputElementRef}
                    type="text" 
                    onChange={this.props.changed} 
                    value={this.props.name}/>
            </Aux>
        
        )
    };
    

}

Person.PropTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age :PropTypes.number,
    changed:PropTypes.func
};
// export default Radium(person);
export default withClass(Person, classes.Person);