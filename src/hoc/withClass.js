import React from 'react';

const withClass = (WrappedComponnent, className) => {
    return props => (
        <div className={className}>
            <WrappedComponnent {...props} />
        </div>
    )
}

export default withClass;